import os.path, shutil, errno
import logging
import logging.handlers
import datetime

class screenSaviour:
	def __init__(self):
		self.image_file = ""
		self.local_google_drive_repository = ""
		#self.UserName = os.getenv("USERNAME")

	def set_log(self):
		
		logger = logging.getLogger('KKPOLog')
		date = datetime.datetime.now().strftime("%d_%m_%Y")
		logging.basicConfig(filename= 'copy_log_'+date+'.log',level=logging.DEBUG,\
		  format='%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s')
			
		return logger

	def verify_files_cp(self,list_file_source, list_file_dest, logger):
		for files in list_file_source:
			if files in list_file_dest:
				list_file_source = filter(lambda x : x != files,list_file_source)
		logger.info('New Files to copy...')
		return list_file_source

	def log_info_list(self,list_source,list_dest, logger):
		logger.info('source')
		logger.info(list_source)
		logger.info('destination')
		logger.info(list_dest)

	def set_files_list(self, source, dest, logger):
		list_source = os.listdir(source)
		list_dest = os.listdir(dest)
		self.log_info_list(list_source, list_dest, logger)			
		list_to_cp = filter(lambda x : x != 'desktop.ini',list_source)
		list_to_cp = self.verify_files_cp(list_to_cp, list_dest, logger)
		return list_to_cp

	def copy(self, source, dest):
		logger = self.set_log()
		if os.path.exists(source) and os.path.exists(dest):
			logger.info('starting copy....')
			try:
				list_to_cp = self.set_files_list(source, dest, logger)
				for files in list_to_cp:
					logger.info('copying '+ files)
					my_file = os.path.join(self.image_file,files)
					shutil.copy2(my_file, self.local_google_drive_repository)
				logger.info('copy finished...')
			except OSError as exc:
				logger.critical('Copy failure')
		else:
			logger.warning('Repository do not exists')

if __name__=="__main__":
	save = screenSaviour()
	save.image_file = os.path.join('C:\Users\Public\Pictures')	#permettre l'utilisateur de modifier le setting de l'application
	save.local_google_drive_repository = os.path.join('C:\Users\EICU-Test\Google Drive\screens')
	save.copy(save.image_file , save.local_google_drive_repository )
